# Renovate Runner

Runs [Renovate](https://docs.renovatebot.com/) on a scheduled pipeline to update dependencies in projects on my GitLab account.